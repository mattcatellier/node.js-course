var Person = require('./lib/Person');

// CUSTOM EVENT AND LISTENER
var ben = new Person('ben')
ben.on('speak', function(message) {
    console.log(`${this.name}: ${message}`)
})
ben.emit('speak', 'Whats up dude?')

// CUSTOM EVENT AND LISTENER
var matt = new Person('matt')
matt.on('speak', function(message) {
    console.log(`${this.name}: ${message}`)
})
matt.emit('speak', 'Not much man, you?')
