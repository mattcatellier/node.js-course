var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
var realPerson = {
    name: '',
    sayings: []
};

rl.question('What is your name? ', function(answer) {
    realPerson.name = answer;
    rl.setPrompt(`What would ${realPerson.name} say? `);
    rl.prompt();
    rl.on('line', function(saying) {
        if(saying.toLowerCase().trim() == 'exit') {
            rl.close();
        }
        realPerson.sayings.push(saying.trim());
        rl.setPrompt(`What else would ${realPerson.name} say? ('exit' to leave) `)
        rl.prompt();
    });
});

rl.on('close', function() {
    console.log('%s is a a ral person that says %j', realPerson.name, realPerson.sayings);
    process.exit();
});
