// LOAD MODULES
var EventEmitter = require('events').EventEmitter;
var util = require('util') // used for inheritance

// CUSTOM OBJECT INHERITS FROM EVENT EMITTER
var Person = function(name) {
    this.name = name
}
util.inherits(Person, EventEmitter)

module.exports = Person;
