var sayings = [
    "How are you?",
    "Hows it going?",
    "How ya going?",
    "Hows it hanging?"
];
// passes data to stdout method
var interval = setInterval(function() {
    var i = Math.floor(Math.random() * sayings.length);
    process.stdout.write(`${sayings[i]} \n`);
}, 1000);

process.stdin.on('data', function(data) {
    console.log(`STDIN -> ${data.toString().trim()}`);
    clearInterval(interval);
    process.exit();
});
