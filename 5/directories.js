var fs = require('fs');


// move file up one level
try {
    fs.renameSync('./assets/logs', './logs');
    console.log("moved logs to root");
} catch(err) {
    console.log(err);
}

// put it back
try {
    fs.renameSync('./logs', './assets/logs');
    console.log("moved logs to assets");
} catch(err) {
    console.log(err);
}

// remove files in log directory
fs.readdirSync('./assets/logs').forEach(function(fileName) {
    fs.unlinkSync('./assets/logs/' + fileName);
    console.log('a.md and b.md removed');
});

// remove 2nd folder with files in it
fs.rmdir('./assets/logs', function(err) {
    if(err) throw err;
    console.log('logs directory removed')

})

// remove 1st folder
fs.rmdir('./assets', function(err) {
    if(err) throw err;
    console.log('assets directory removed')
})

// create the same file structure again
fs.mkdir('./assets', function(err) {
    if(err) throw err;
    console.log("Assets Directory created");
});

fs.mkdir('./assets/logs', function(err) {
    if(err) throw err;
    console.log("logs Directory created");
});

fs.writeFile('./assets/logs/a.md', 'a', function(err) {
    if(err) throw err;
    console.log("a.md File created");
});

fs.writeFile('./assets/logs/b.md', 'b', function(err) {
    if(err) throw err;
    console.log("b.md File created");
});
