var fs = require('fs');

console.log('--- started reading file');
fs.readFile('./chat.log', 'UTF-8', function(err, chatlog) {
    console.log('\n--- done reading file ' + chatlog.length);
});


var stream = fs.createReadStream("./chat.log", "UTF-8");
var data = '';

stream.once('data', function() {
    console.log('\n\n\n');
    console.log('started reading file with stream');
    console.log('\n\n\n');
})
stream.on('data', function(chunk) {
    process.stdout.write(`  chunk: ${chunk.length} |`);
    data += chunk;
})

stream.on('end', function() {
    console.log('\n\n\n');
    console.log('finished reading file stream ' + data.length);
    console.log('\n\n\n');
})
