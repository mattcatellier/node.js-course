const fs = require('fs');

fs.renameSync('./lib/project-config.js', './lib/config.json');
console.log('Config file renamed');

fs.rename("./lib/notes.md", './notes.md', function(err) {
    if(err) {
        console.log(err);
        return;
    }
    console.log('file moved');
});
