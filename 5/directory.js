var fs = require('fs');

if(fs.existsSync('lib')) {
    console.log('Directory exists');
    return;
}

fs.mkdir('lib', function(err) {
    if(err) {
        console.log(err)
        return;
    }
    console.log("Directory created");
});
