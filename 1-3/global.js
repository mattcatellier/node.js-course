// use back ticks to escape vairable names
var hello = "Hello World from Node js";
var justNode = hello.slice(17);
console.log(`Rock on from ${justNode}`);

// paths to files
console.log(__dirname);// path to file, no filename
console.log(__filename);// path to file, with filename

// load in modules, how to do multiple?
var path = require("path");
console.log(`Rock on World from ${path.basename(__filename)}`)