var questions = [
	"What is your name?",
	"What is your favourite band?",
	"What is your favourite food?"
];

var answers = [];


function ask(i) {
    process.stdout.write(`\n\n\n ${questions[i]}`);
    process.stdout.write("  >  ");
}

process.stdin.on('data', function(data) {
    // process.stdout.write('\n' + data.toString().trim() + '\n');
    answers.push(data.toString().trim())
    if(answers.length < questions.length) {
        ask(answers.length); // will ask next question
    } else {
        process.exit();
    }
})

process.on('exit', function() {
    for(var i = 0; i < answers.length; i++) {
        process.stdout.write('\n\n');
        process.stdout.write(`${questions[i]} \n -> ${answers[i]}`)
    }
})

ask(0);
