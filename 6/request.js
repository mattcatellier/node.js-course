var https = require('https')
var fs = require('fs')

// https://cloud.google.com/nodejs/getting-started/using-pub-sub
var options = {
    hostname: "cloud.google.com", // i.e cloud.google.com
    port: 443,
    path: "/nodejs/getting-started/using-pub-sub", // i.e. /nodejs/getting-started/using-pub-sub
    method: "GET"
}

var req = https.request(options, function(res) {
    var responseBody = ""

    console.log('response recieved')
    console.log(`HTTP Status: ${res.statusCode}`)
    console.log('request headers: %j', res.headers)

    res.setEncoding("UTF-8")
    res.once("data", function(chunk) {
        console.log(chunk)
    })

    res.on("data", function(chunk) {
        console.log(`--chunk-- ${chunk.length}`)
        responseBody += chunk
    })

    res.on("end", function() {
        fs.writeFile("using-pub-sub.html", responseBody, function(err) {
            if(err) {
                throw err
            }
            console.log("File Downloaded")
        })
    })
})

req.on("error", function(err) {
    console.log(`problem with request ${err.message}`)
})

req.end();
