var http = require("http");
var data = require("./data/inventory");

http.createServer(function(req, res) {
    res.writeHead(200, {
        "Content-Type": "text/json"
    })
    if(req.url === "/") {
        res.end(JSON.stringify(data));
    } else if(req.url === "/instock") {
        listInStock(res);
    } else if(req.url === "/backorder") {
        listOnBackorder(res);
    } else {
        res.writeHead(404, {
            "Content-Type": "text/plain"
        });
        res.end('Data not found.');
    }


}).listen(3000);

console.log('port 3000');


function listInStock(res) {
    var inStock = data.filter(function(item) {
        return item.avail;
    });
    res.end(JSON.stringify(inStock));
}

function listOnBackorder(res) {
    var onOrder = data.filter(function(item) {
        return !item.avail;
    });
    res.end(JSON.stringify(onOrder));
}
